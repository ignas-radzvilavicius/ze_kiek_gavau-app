package lt.akademija;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;


public class CarRepo implements Serializable {

	private static final long serialVersionUID = -9143803268325484761L;

	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;
	
	
	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.entityManager = entityManagerFactory.createEntityManager();
	}
	
	
	public Car searchById(Long idToFind) {
		entityManager = entityManagerFactory.createEntityManager();
		TypedQuery<Car> carQuery = entityManager.createQuery("SELECT i FROM Car i WHERE i.id = :id", Car.class);
		carQuery.setParameter("id", idToFind);
		carQuery.setMaxResults(1);
		return carQuery.getSingleResult();
	}
	
	public void save(Car carToSave) {
		entityManager = entityManagerFactory.createEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(carToSave)) {
				carToSave = entityManager.merge(carToSave);
			}
			entityManager.persist(carToSave);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}
	
	public void delete(Car carToDelete) {
		entityManager = entityManagerFactory.createEntityManager();
		try {
			entityManager.getTransaction().begin();
			carToDelete = entityManager.merge(carToDelete);
			entityManager.remove(carToDelete);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}
}
