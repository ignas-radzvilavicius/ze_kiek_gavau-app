package lt.akademija;

import java.io.Serializable;
import java.util.List;

public class TrainController implements Serializable {

	private static final long serialVersionUID = 3660758841682728290L;

	private TrainRepo trainRepo;
	private CurrentTrain currentTrain;
	

	public void add() {
		currentTrain.setSelectedTrain(new Train());
	}
	
	public void update(Long trainToUpdateId) {
		currentTrain.setSelectedTrain(trainRepo.searchById(trainToUpdateId));
	}
	
	public void delete(Train trainToDelete) {
		trainRepo.delete(trainToDelete);
	}
	
	public void cancel() {
		currentTrain.setSelectedTrain(new Train());
	}
	
	public void save() {
		trainRepo.save(currentTrain.getSelectedTrain());
	}
	
	public void viewTrain(Long trainId) {
		currentTrain.setSelectedTrain(trainRepo.searchById(trainId));
	}
	
	public List<Car> cars() {
		return trainRepo.searchById(
				currentTrain.getSelectedTrain().getNumber()).getCars();
	}

	public TrainRepo getTrainRepo() {
		return trainRepo;
	}

	public void setTrainRepo(TrainRepo trainRepo) {
		this.trainRepo = trainRepo;
	}

	public CurrentTrain getCurrentTrain() {
		return currentTrain;
	}

	public void setCurrentTrain(CurrentTrain currentTrain) {
		this.currentTrain = currentTrain;
	}

	
	
}
