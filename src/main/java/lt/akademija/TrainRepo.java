package lt.akademija;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import lt.akademija.Train;

public class TrainRepo implements Serializable {

	private static final long serialVersionUID = 5499045175235083753L;

	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;
	
	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.entityManager = entityManagerFactory.createEntityManager();
	}
	
	public List<Train> findAll() {
		entityManager = entityManagerFactory.createEntityManager();
		TypedQuery<Train> trainQuery = entityManager.createQuery("SELECT i FROM Train i", Train.class);
		return trainQuery.getResultList();
	}
	
	public Train searchById(Long idToFind) {
		TypedQuery<Train> trainQuery = entityManager.createQuery("SELECT i FROM Train i WHERE i.id = :id", Train.class);
		trainQuery.setParameter("id", idToFind);
		trainQuery.setMaxResults(1);
		return trainQuery.getSingleResult();
	}
	
	public void save(Train trainToSave) {
		entityManager = entityManagerFactory.createEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(trainToSave)) {
				trainToSave = entityManager.merge(trainToSave);
			}
			entityManager.persist(trainToSave);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}
	
	public void delete(Train trainToDelete) {
		entityManager = entityManagerFactory.createEntityManager();
		try {
			entityManager.getTransaction().begin();
			trainToDelete = entityManager.merge(trainToDelete);
			entityManager.remove(trainToDelete);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

}
