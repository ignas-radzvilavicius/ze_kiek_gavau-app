package lt.akademija;

public enum CarType {
	PASSENGER("Keleivinis"), FREIGHT("Krovininis"), LOCOMOTIVE("Lokomotyvas");

	private String type;

	private CarType(String s) {
		type = s;
	}

	public String getType() {
		return type;
	}
}
