package lt.akademija;

import java.io.Serializable;


public class CarController implements Serializable {

	private static final long serialVersionUID = 6084841471275381994L;

	private CarRepo carRepo;
	private CurrentCar currentCar;
	
	private TrainRepo trainRepo;
	private CurrentTrain currentTrain;
	
	
	public void add() {
		currentCar.setSelectedCar(new Car());
	}
	
	public void update(Long carToUpdateId) {
		currentCar.setSelectedCar(carRepo.searchById(carToUpdateId));
	}
	
	public void delete(Car carToDelete) {
		carRepo.delete(carToDelete);
	}
	
	public void cancel() {
		currentCar.setSelectedCar(new Car());
	}
	
	public void save() {
		currentCar.getSelectedCar().setTrain(currentTrain.getSelectedTrain());
		carRepo.save(currentCar.getSelectedCar());
	}

	
	public CarRepo getCarRepo() {
		return carRepo;
	}

	public void setCarRepo(CarRepo carrepo) {
		this.carRepo = carrepo;
	}

	public CurrentCar getCurrentCar() {
		return currentCar;
	}

	public void setCurrentCar(CurrentCar currentCar) {
		this.currentCar = currentCar;
	}

	public TrainRepo getTrainRepo() {
		return trainRepo;
	}

	public void setTrainRepo(TrainRepo trainRepo) {
		this.trainRepo = trainRepo;
	}

	public CurrentTrain getCurrentTrain() {
		return currentTrain;
	}

	public void setCurrentTrain(CurrentTrain currentTrain) {
		this.currentTrain = currentTrain;
	}
}
