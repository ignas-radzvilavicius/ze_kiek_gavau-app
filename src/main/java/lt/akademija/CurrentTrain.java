package lt.akademija;

import java.io.Serializable;

public class CurrentTrain implements Serializable {

	private static final long serialVersionUID = 4015272796732152183L;

	private Train selectedTrain;

	
	public Train getSelectedTrain() {
		return selectedTrain;
	}

	public void setSelectedTrain(Train selectedTrain) {
		this.selectedTrain = selectedTrain;
	}
}
