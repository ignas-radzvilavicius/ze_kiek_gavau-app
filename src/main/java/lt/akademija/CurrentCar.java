package lt.akademija;

import java.io.Serializable;

public class CurrentCar implements Serializable {

	private static final long serialVersionUID = 5099581964805791364L;
	
	private Car selectedCar;

	public Car getSelectedCar() {
		return selectedCar;
	}

	public void setSelectedCar(Car selectedCar) {
		this.selectedCar = selectedCar;
	}
	
}
